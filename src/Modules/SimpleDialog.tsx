import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
} from "@mui/material";

export interface SimpleDialogProps {
  open: boolean;
  score: number;
  onCloseDialog: React.Dispatch<React.SetStateAction<boolean>>;
  resetInformation: () => void;
}

function SimpleDialog(props: SimpleDialogProps) {
  const { open, score, onCloseDialog, resetInformation } = props;

  const handleClose = () => {
    onCloseDialog(false);
    resetInformation();
  };

  return (
    <Grid>
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={open}
        fullWidth
        maxWidth="xs"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          style={{ fontSize: "1.8rem", color: "red" , fontWeight:"bold"}}
        >
          {"Game Over"}
        </DialogTitle>

        <DialogContent>
          <DialogContentText id="alert-dialog-description" style={{ fontSize: "1.2rem", color: "black" }}>
            Your Score: {score}
          </DialogContentText>
          <DialogContentText id="alert-dialog-description" style={{ fontSize: "1.1rem", color: "black" }}>
            Sorry, Try Again !!!
          </DialogContentText>
        </DialogContent>

        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Start Again
          </Button>
        </DialogActions>
      </Dialog>
    </Grid>
  );
}

export default SimpleDialog;
