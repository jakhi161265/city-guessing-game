import { Grid, Paper, Typography } from "@mui/material";
import React, { useState, useEffect } from "react";
import ReactMapGL, { Marker } from "react-map-gl";
import { mapAPI } from "../config";
import { IoIosPin, IoMdInformation } from "react-icons/io";
import SimpleDialog from "./SimpleDialog";

const allCities = [
  {
    name: "Amsterdam",
    position: {
      lat: 52.370216,
      lng: 4.895168,
    },
  },
  {
    name: "Rome",
    position: {
      lat: 41.902783,
      lng: 12.496366,
    },
  },
  {
    name: "Helsenki",
    position: {
      lat: 60.169856,
      lng: 24.938379,
    },
  },
  {
    name: "Stockholm",
    position: {
      lat: 59.329323,
      lng: 18.068581,
    },
  },
  {
    name: "London",
    position: {
      lat: 51.507351,
      lng: -0.127758,
    },
  },
  {
    name: "Oslo",
    position: {
      lat: 59.913869,
      lng: 10.752245,
    },
  },
  {
    name: "Paris",
    position: {
      lat: 48.856614,
      lng: 2.352222,
    },
  },
  {
    name: "Wien",
    position: {
      lat: 48.208174,
      lng: 16.373819,
    },
  },
  {
    name: "Budapest",
    position: {
      lat: 47.497912,
      lng: 19.040235,
    },
  },
];

const MapGame = () => {
  const [index, setIndex] = useState(0);
  const [isCorrect, setIsCorrect] = useState(false);

  // map information
  const [viewPort, setViewPort] = useState({
    latitude: 51.87437975470169,
    longitude: 10.319717870382846,
    width: "290vh",
    height: "80vh",
    zoom: 4,
  });

  const initialInformation = {
    initialDistance: 1500,
    cityFoundScore: 0,
    calculatedDistance: 0,
    targetCityName: allCities[index]?.name,
    targetCityLat: allCities[index]?.position?.lat,
    targetCityLng: allCities[index]?.position?.lng,
    clickedCityLat: 0,
    clickedCityLng: 0,
  };

  const [information, setInformation] = useState(initialInformation);

  // marker information
  const [marker, setMarker] = useState({
    latitude: 51.87437975470169,
    longitude: 10.319717870382846,
  });

  const [isMarkerShow, setIsMarkerShow] = useState(false);
  const [open, setOpen] = React.useState(false);

  function getDistanceFromLatLonInKm(
    lat1: any,
    lon1: any,
    lat2: any,
    lon2: any
  ) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);

    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(lat1)) *
        Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);

    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km

    return d;
  }

  function deg2rad(deg: any) {
    return deg * (Math.PI / 180);
  }

  const handleClickOpen = () => {
    setOpen(true);
  };

  const reset: () => void = () => {
    setViewPort({
      ...viewPort,
      zoom:5,
      longitude: 10.319717870382846,
      latitude: 51.87437975470169,
    });
    setIsMarkerShow(false);
    setInformation(initialInformation);
    setIndex(0);
    setIsCorrect(false);
  };

  const handleFindCity = (event: any) => {
    setIsMarkerShow(true);
    setIsCorrect(false);

    setMarker({
      longitude: event.lngLat[0],
      latitude: event.lngLat[1],
    });

    setViewPort({
      ...viewPort,
      zoom: 4,
      longitude: event.lngLat[0],
      latitude: event.lngLat[1],
    });

    setInformation({
      ...information,
      clickedCityLat: event.lngLat[1],
      clickedCityLng: event.lngLat[0],
    });

    // get the distance between needle pin and the target city
    const calculatedDistance: number = getDistanceFromLatLonInKm(
      information?.targetCityLat,
      information?.targetCityLng,
      event?.lngLat[1],
      event?.lngLat[0]
    );

    const strinfDistance = calculatedDistance?.toFixed(2);
    const distance = parseFloat(strinfDistance);

    const updatedDistance = information?.initialDistance - distance;

    if (distance > 50 && updatedDistance > 0) {
      // couldn't find city yet but still have point to play the game
      setInformation({
        ...information,
        calculatedDistance: distance,
        initialDistance: updatedDistance,
      });
    } else if (distance <= 50 && updatedDistance > 0) {
      // find city and have point to play the game to find next city
      if (index < allCities.length - 1) {
        // more cities are left so,generate nexxt city index and set on the information object
        const updatedIndex = index + 1;
        setInformation({
          ...information,
          calculatedDistance: distance,
          initialDistance: information?.initialDistance - distance,
          cityFoundScore: information.cityFoundScore + 1,
          targetCityName: allCities[updatedIndex]?.name,
          targetCityLat: allCities[updatedIndex]?.position?.lat,
          targetCityLng: allCities[updatedIndex]?.position?.lng,
        });
        setIndex(updatedIndex);
        setIsCorrect(true);
      } else {
        // no city is left so, show the final score
        setInformation({
          ...information,
          calculatedDistance: distance,
          initialDistance: information?.initialDistance - distance,
          cityFoundScore: information.cityFoundScore + 1,
        });
        setIsCorrect(true);
        handleClickOpen();
      }
    } else if (updatedDistance < 0) {
      // reset all the information and show the final score
      setInformation({
        ...information,
        initialDistance: 0,
        calculatedDistance: distance,
      });
      handleClickOpen();
    }
  };

  useEffect(() => {
    if (index) {
      setInformation({
        ...information,
        targetCityName: allCities[index]?.name,
        targetCityLat: allCities[index]?.position?.lat,
        targetCityLng: allCities[index]?.position?.lng,
        clickedCityLat: 0,
        clickedCityLng: 0,
      });
    }
  }, [index]);

  return (
    <>
      {/* Data Section */}
      <Grid style={{ marginTop: "1rem" }}>
        <Paper
          elevation={3}
          style={{ marginTop: "1rem", padding: "1rem 2rem" }}
        >
          <Grid
            container
            spacing={2}
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
          >
            {/* Information Section */}
            <Grid item xs={4}>
              <Typography
                style={{
                  color: "white",
                  fontWeight: "bold",
                  fontSize: "1.2rem",
                  backgroundColor: "#ab47bc",
                  border: "4px solid #9c27b0",
                  borderRadius: "5px",
                  padding: "0rem 5rem",
                }}
              >
                Guessing City : {allCities[index]?.name}
              </Typography>
            </Grid>

            <Grid item xs={4}>
              <Typography
                style={{
                  color: "white",
                  fontWeight: "bold",
                  fontSize: "1.2rem",
                  backgroundColor: "#ab47bc",
                  border: "4px solid #9c27b0",
                  borderRadius: "5px",
                  padding: "0rem 5rem",
                }}
              >
                Distance Left : {information?.initialDistance?.toFixed(2)}{" "}
                Km
              </Typography>
            </Grid>

            <Grid item xs={4}>
              <Typography
                style={{
                  color: "white",
                  fontWeight: "bold",
                  fontSize: "1.2rem",
                  backgroundColor: "#ab47bc",
                  border: "4px solid #9c27b0",
                  borderRadius: "5px",
                  padding: "0rem 2rem",
                }}
              >
                Distance Between Two Cites:{" "}
                {information?.calculatedDistance?.toFixed(2)} Km
              </Typography>
            </Grid>

            <Grid item xs={2}>
              <Typography
                style={{
                  color: "white",
                  fontWeight: "bold",
                  fontSize: "1.5rem",
                  backgroundColor: "#536dfe",
                  border: "4px solid #3a4cb1",
                  borderRadius: "5px",
                  padding: "0rem 1rem",
                }}
              >
                Your Score : {information?.cityFoundScore}
              </Typography>
            </Grid>

            <Grid item xs={4}>
              {isCorrect && (
                <Typography
                  style={{
                    color: "white",
                    fontWeight: "bold",
                    fontSize: "1.5rem",
                    backgroundColor: "#43a047",
                    border: "4px solid #2e7d32",
                    borderRadius: "5px",
                    padding: "0rem 2rem",
                    margin: "0rem 10rem",
                  }}
                >Correct</Typography>
              )}
            </Grid>

            <Grid item xs={4}>
              {!isCorrect && isMarkerShow && (
                <Typography
                  style={{
                    color: "white",
                    fontWeight: "bold",
                    fontSize: "1.5rem",
                    backgroundColor: "#e53935",
                    border: "4px solid #b71c1c",
                    borderRadius: "5px",
                    padding: "0rem 2rem",
                    margin: "0rem 10rem",
                  }}
                >Wrong</Typography>
              )}
            </Grid>

            {/* Map Section */}
            <Grid item xs={12}>
              <div>
                <Paper elevation={3}>
                  <Grid container justifyContent="center" style={{border:"3px solid blue",  borderRadius: "5px",}}>
                    <ReactMapGL
                      data-testid="map"
                      {...viewPort}
                      mapStyle="mapbox://styles/mapbox/dark-v8"
                      mapboxApiAccessToken={mapAPI}
                      onViewportChange={(viewport: any) => {
                        setViewPort(viewport);
                      }}
                      onDblClick={handleFindCity}
                    >
                      {isMarkerShow && (
                        <Marker
                          latitude={marker.latitude}
                          longitude={marker.longitude}
                        >
                          <IoIosPin
                            style={{ fontSize: "4rem", color: "red" }}
                          />
                        </Marker>
                      )}
                    </ReactMapGL>
                  </Grid>
                </Paper>
              </div>
            </Grid>
          </Grid>
        </Paper>

        {open && (
          <SimpleDialog
            open={open}
            score={information.cityFoundScore}
            onCloseDialog={setOpen}
            resetInformation={reset}
          />
        )}
      </Grid>
    </>
  );
};

export default MapGame;
