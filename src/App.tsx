import React from 'react';
import MapGame from './Modules/MapGame';

function App() {
  return (
    <div className="App">
      <MapGame />
    </div>
  );
}

export default App;
