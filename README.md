# City Guessing-Game

![Semantic description of image](/city-guessing-game.png)_City Guessing Game_

[Click Here To Play The Game](https://6293706eb0f7b6480d4a91f7--the-awesome-jobeda123-site.netlify.app/)

## Description
This is a city guessing game. User can guess city and can obtain point. Initially user have 1500km in total distance. Everytime one city name will appear in the top left board and user have to guess that city. If distance between 2 cites is less than 50km then user will get one point and and reduce the calculated distance from the total distance. As user guessing every city correctly then number of correct guessing cites is the score of this game.

## How To Play
- Double click to select a place
- Drag the map to move from one place to another
- Can zoom out and zoom in the map
- Will increase score if guessing is correct
- Will show correct indicator on top; if guess is correct
- Will show wrong indicator on top; if guess is wrong
- Will final score when user doesn't have enough distance or guess all the cities name correctly